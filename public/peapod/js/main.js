$( function() {
  $( document ).foundation();

  $scroller = $( '#onepage-scroll' );
  $bgVideo = $( '#bg-video' );

  $( '#hp-scroll-down' ).click( function( e ) {
    e.preventDefault();
    $scroller.moveDown();
  });
  $( '#hp-scroll-up' ).click( function( e ) {
    e.preventDefault();
    $scroller.moveUp();
  });

  var $svgIconCont = $( '#svg-icon-cont' );

  var getVisiblePageIndex = function() {
    var classList = $( 'body' ).attr( 'class' );
    if ( classList.search( 'viewing-page-' ) !== -1 )
      return parseInt( classList[ classList.length - 1 ]);
    else
      conosle.log( "Couldn't find current page index." );
  };

  $benefitsPage = $( '#benefits-pg' );
  $careersContainer = $( '#cc-cont' );
  var manageScrollArrowVisibility = function() {
    if ( $careersContainer.hasClass( 'fadeInRight' )) return;
    if ( $benefitsPage.hasClass( 'fadeInRightBig' )) return;
    var classList = $( 'body' ).attr( 'class' );
    if ( classList.search( 'viewing-page-' ) !== -1 ) {
      var opsIndex = parseInt( classList[ classList.length - 1 ]);
      if ( opsIndex !== 1 ) $( '#hp-scroll-up' ).show();
      if ( opsIndex === 4 || opsIndex === 7 ) {
        $( '#hp-scroll-down, #hp-scroll-up' ).attr( 'src', 'img/scroll-down-black.png' );
      }
      else {
        $( '#hp-scroll-down, #hp-scroll-up' ).attr( 'src', 'img/scroll-down.png' );
      }
      if ( opsIndex !== 7 ) $( '#hp-scroll-down' ).show();
      else $( "footer .truck" ).hide();
      // return parseInt( classList[ classList.length - 1 ]);
    }
  };

  var hideScrollArrows = function() {
    $( '#hp-scroll-up' ).hide();
    $( '#hp-scroll-down' ).hide();
  };

  $truck = $( "footer .truck" );
  if ( $('#onepage-scroll' ).length ) {
    $( '#onepage-scroll' ).onepage_scroll({
      loop: false,
      updateURL: true,
      beforeMove: function( index ) {
        hideScrollArrows();
      },
      afterMove: function( index ) {
        manageScrollArrowVisibility();
        $currentScrollSlide = $( '.ops-section[data-index=' + index + ']' );
        var transition = $currentScrollSlide.find( '.disp-tcell-vm' ).data( 'intro-transition' ) || 'fadeInLeft';
        $currentScrollSlide.find( '.hidden' ).removeClass( 'hidden' ).addClass( 'animated ' + transition );
        if ( getVisiblePageIndex() === 7 ) {
          showTruckAnimation();
        }
        else {
          $truck.hide();
        }
      }
    });
  }


  // Text animation effects for home page. -------------------------------------
  $( '#onepage-scroll h1.hidden' ).removeClass( 'hidden' ).addClass( 'animated fadeInUp' );
  $( '#onepage-scroll .btn.hidden' ).removeClass( 'hidden' ).addClass( 'animated fadeInUp' );
  $('#onepage-scroll').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
      $( '#onepage-scroll .animated' ).removeClass( 'animated' ).removeClass( 'fadeInUp');
  });


  // Careers page curtain ------------------------------------------------------
  $curtains = $( '#career-curtain' );
  $curtainLis = $( '#career-curtain > li' );
  $topBar = $( '.top-bar-customizations' );

  var setCurtainHeight = function() {
    $curtainLis.css( 'min-height', $( window ).height());
  };
  setCurtainHeight();

  
  var mobileViewForCurtains = function( that ) {
    $( that ).find( 'h3' ).addClass( 'soa hidden' );
    var mcdID = Date.now();
    var backBtn = '';
    $( '#mbb-cont > article' ).html( '<div class="mc-details" id="' + mcdID + '">' + $( that ).find( '.caption' ).html() + backBtn + '</div>' );
    $( '#mbb-cont' ).show().css( 'opacity', 1 );
    $( '#mbb-cont' ).removeClass( 'fadeOutRightBig' ).addClass( 'animated fadeInRightBig' );
    $( '#mbb-cont .soa' ).removeClass( 'hidden' ).velocity( 'transition.slideDownIn', { stagger: 250 });
  };

  $dispTable = $( '#onepage-scroll .disp-table' );
  $dispTable.css( 'height', $( window ).height() - parseInt($( '.top-bar-customizations' ).css( 'height' )));
  $( window ).resize( function() {
    if ( $( window ).width() >= 768 )
      setCurtainHeight();
    $dispTable.css( 'height', $( window ).height() - parseInt($topBar.css( 'height' )));
  });

  $( '.show-cmain' ).on( 'click', function( e ) {
    e.preventDefault();
    //$( '#mbb-cont' ).addClass( 'animated fadeOutRightBig' );
    $( '#career-curtain h3' ).removeClass( 'hidden' ).velocity( 'transition.bounceUpIn' );
    $( '#mbb-cont' ).removeClass( 'fadeInRightBig' ).addClass( 'animated fadeOutRightBig' );
    $( '#mbb-cont > article' ).html( '' );
  });

  $( '.close-benefits-page' ).on( 'click', function( e ) {
    e.preventDefault();
    $( 'body' ).removeClass( 'disabled-onepage-scroll' );
    $benefitsPage.removeClass( 'fadeInRightBig' ).addClass( 'animated fadeOutRightBig' );
  });

  $curtainLis.mouseenter( function(){
    if ( $( window ).width() < 768 ) return;
    if ( $curtains.find( '.active' ).length ) return;
    $curtainLis.addClass( 'shrink-this' );
    $( this ).removeClass( 'shrink-this' );
    $( '.shrink-this' ).css( 'width', '19%' );
    $( this ).css( 'width', '24%' );
  }).mouseleave( function() {
    resetCurtains();
  });

  var expandCurtains = function( that ) {
    if ( $( window ).width() < 768 ) {
      mobileViewForCurtains( that );
      return;
    }
    if ( $( that ).hasClass( 'active' )) {
      resetCurtains();
      $( that ).removeClass( 'active' );
      return;
    }
    $curtainLis.addClass( 'tab-this' );
    $( that ).removeClass( 'tab-this' );
    $curtainLis.addClass( 'shrink-this' ).removeClass( 'active' );
    $( that ).removeClass( 'shrink-this' ).addClass( 'active' );
    $( '.active .bg-main img' ).animate({ 'opacity': 0 });
    $( '.active h3' ).hide();
    $( 'li.active .soa' ).hide();
    $( '#career-curtain li' ).not( 'active' ).find( 'h3' ).hide();
    setTimeout( function() {
      // Animate after <li> width animation is done.
      $( 'li.active .soa' ).removeClass( 'hidden' ).velocity( 'transition.slideDownIn', { stagger: 250 });
    }, 800 );
    $( '.shrink-this' ).css( 'width', '10%' );
    $( that ).css( 'width', '60%' );
  };

  var resetCurtains = function( override ) {
    if ( $( window ).width() < 768 ) return;
    if ( $curtains.find( '.active' ).length && override !== 'mustReset' ) return;
    $( '.soa' ).not( 'h3' ).addClass( 'hidden' ).hide();
    $( 'h3.soa' ).show();
    $curtainLis.css( 'width', '20%' ).removeClass( 'active' );
    $curtainLis.removeClass( 'tab-this' );
  };

  $curtainLis.click( function( e ){
    if( !$( e.target ).hasClass( 'btn' )) e.preventDefault();
    if ( $curtains.find( '.active' ).length ) {
      resetCurtains( 'mustReset' );
      return;
    }
    expandCurtains( this );
  });


  // Footer --------------------------------------------------------------------
  var showTruckAnimation = function() {
    var left = parseInt( $truck.css( 'left' ));
    $truck.fadeIn( function() {
      $truck.css( 'animation-name', 'truckAnimation' ).css( '-webkit-animation-name', 'truckAnimation' );
    });
  };

  $( '.show-careers-cont' ).click( function( e ) {
    e.preventDefault();
    hideScrollArrows();
    setTimeout( function() {
      // Play SVG icon animation after <li> width animation is done.
      $( '#cc-cont embed' ).removeClass( 'hidden' ).addClass( 'animated fadeInRight' );
    }, 900 );
    $careersContainer.show().removeClass( 'fadeOutRight' ).addClass( 'animated fadeInRight' );
  });

  $( '#close-curtains' ).click( function( e ) {
    e.preventDefault();
    $( '#cc-cont embed' ).addClass( 'hidden' );
    $careersContainer.show().removeClass( 'fadeInRight' ).addClass( 'animated fadeOutRight' );
    setTimeout( function() {
      resetCurtains( 'mustReset' );
      manageScrollArrowVisibility();
    }, 1100 );
  });


  /* Video Gallery ---------------------------------------------------------- */
  $videoGallery = $( '.video-gal' );
  $videoCont = $( '.video-gal article' );
  $nextVideo = $( '#next-video' );
  $prevVideo = $( '#prev-video' );
  var videos = {};
  $( '#video-gallery [data-video-index]' ).each( function() {
    videos[$( this ).data( 'video-index' )] = $( this ).data( 'video-url' );
  });

  $( 'body' ).on( 'click', '.vg-thumb', function( e ) {
    e.preventDefault();
    hideScrollArrows();
    $videoGallery.removeClass( 'fadeOutRight' ).addClass( 'animated fadeInRight' );
    var videoMarkup = '<iframe id="ytplayer" type="text/html" width="640" height="390" src="' + $( this ).data( 'video-url' ) + '?autoplay=1" frameborder="0" allowfullscreen data-video-index="' + $( this ).data( 'video-index' ) + '"/>';
    setTimeout( function() {
      $videoCont.html( videoMarkup );
      $videoCont.fitVids();
    }, 1100 );
    hideScrollArrows();
    //$( '.video-nav' ).show();
  });
  
  $( '#vgal-close' ).click( function( e ) {
    e.preventDefault();
    $( '.video-nav' ).hide();
    $videoGallery.removeClass( 'fadeInRight' ).addClass( 'animated fadeOutRight' );
    $videoCont.html( '' );
    manageScrollArrowVisibility();
  });

  //$( '#video-gallery' )
  $nextVideo.click( function( e ) {
    e.preventDefault();
    hideScrollArrows();
    var vi = parseInt( $( '.video-gal video' ).data( 'video-index' ));
    console.log( vi );
    vi++;
    $( '.video-gal video' ).attr( 'data-video-index', vi++ );
  });

  $prevVideo.click( function( e ) {
    e.preventDefault();
    hideScrollArrows();
    var videoIndex = 0;
    videoIndex = parseInt( $( '.video-gal video' ).data( 'video-index' ));
    if ( videoIndex === 1 ) {
      console.log( 'This is the first video!' );
      $prevVideo.hide();
      return;
    }
    $prevVideo.show();
    console.log( videoIndex, parseInt( $( '.video-gal video' ).data( 'video-index' )));
    videoIndex = videoIndex - 1;
    var videoSrc = $( '#video-gallery' ).find( '[data-video-index="' + videoIndex + '"]' ).data( 'video-url' )
    console.log( videoIndex, videoSrc );
    $( '.video-gal video' ).attr( 'src', videoSrc );
    console.log( videoIndex );
    $( '.video-gal video' ).attr( 'data-video-index', videoIndex );
    x = document.querySelectorAll( '.video-gal video' )[0];
    x.setAttribute( 'data-video-index', videoIndex )
  });

  if ( $( 'body.about-us' ).length ) {
    // var paddingTop = $( '.top-bar-customizations' ).height() + parseInt( $( '.about-us-hdg' ).css( 'height' ));
    // $( 'body' ).css( 'padding-top', paddingTop );

    $( '.peapod-way .row' ).each( function( i, v ) {
      if ( parseInt( $( this ).find( '.large-9' ).css( 'height' )) < parseInt( $( this ).find( '.large-3' ).css( 'height' ))) {
        $( this ).find( '.large-9, p' ).css( 'height', $( this ).find( '.large-3' ).css( 'height' ));
      }
    });

    $( window ).scroll( function() {
      if ( $( window ).scrollTop() > 100 ) {
        // $( '.about-us-hdg' ).css( 'padding', '10px 0' );
        $( '.about-us-hdg' ).addClass( 'about-us-hdg-compact' );
      }
      else {
        // $( '.about-us-hdg' ).css( 'padding', '100px 0' );
        $( '.about-us-hdg' ).removeClass( 'about-us-hdg-compact' );
      }
    });
  }

  // Benefits page.
  $( '#view-benefits' ).click( function( e ) {
    e.preventDefault();
    hideScrollArrows();
    $benefitsPage.show().removeClass( 'fadeOutRightBig' ).addClass( 'animated fadeInRightBig' );
    setTimeout( function() {
      $( 'body' ).addClass( 'disabled-onepage-scroll' );
    }, 1100 );
  });
});