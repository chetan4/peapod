Static pages for careers site of Peapod
=======================================

Getting Started
---------------

### Viewing the Project
Just open **index.html** at the root of this repository in your browser.


### Setup for Development
Ensure you have [Node.js](http://nodejs.org/) and [Grunt.js](http://gruntjs.com/) installed. Node is used for installing some of the dependencies. Grunt is used for configuring Sass, and a bunch of other tasks.

Use these commands at the terminal:
**npm install** (Install dependencies)
**grunt watch** (This task runs in background, watches for changes to Sass files, and compiles Sass to CSS.)
