module.exports = function(grunt) {
  grunt.initConfig({
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'sass',
          src: ['*.sass'],
          dest: 'css',
          ext: '.css'
        }]
      }
    },

    compass: {                  // Task
      dist: {                   // Target
        options: {              // Target options
          sassDir: 'sass',
          cssDir: 'css',
          environment: 'production'
        }
      },
      dev: {                    // Another target
        options: {
          sassDir: 'css',
          cssDir: 'css'
        }
      }
    },

    jshint: {
      // all: ['Gruntfile.js', 'lib/**/*.js', 'test/**/*.js']
      all: ['Gruntfile.js', 'js/*.js', 'test/**/*.js']
    },

    watch: {
      sass: {
        // We watch and compile sass files as normal but don't live reload here
        files: ['sass/*.sass'],
        tasks: ['sass'],
      },
      livereload: {
        // Here we watch the files the sass task will compile to
        // These files are sent to the live reload server after sass compiles to them
        options: { livereload: true },
        files: ['css/*.css'],
      },
    }
  });


  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['sass', 'jshint', 'compass']);
};
